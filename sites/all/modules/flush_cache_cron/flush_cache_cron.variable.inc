<?php

/**
 * Implements hook_variable_info().
 */
function flush_cache_cron_variable_info($options) {
  $variables['flush_cache_cron_interval'] = array(
    'title' => t('Flush Cache Cron interval'),
    'type' => 'number',
    'default' => 10800,
    'description' => t('The interval in seconds between Flush Cache Cron runs. Enter 0 for no cron run.'),
    'group' => 'flush_cache_cron',
  );
  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function flush_cache_cron_variable_group_info() {
  $groups['flush_cache_cron'] = array(
    'title' => t('Flush Cache Cron'),
    'description' => t('Interval in seconds between cron runs of Flush Cache Cron. 0 for no cron runs.'),
    'access' => 'administer fush cache cron',
  );
  return $groups;
}
