<?php

function bootstrap_child_theme() {

    $templatePath = path_to_theme() . '/templates';
    $themes = [];
    
    $themes['stage3eme_home_template'] = [
        'template' => 'homepage',
        'path' => $templatePath
    ];
    $themes['stage3eme_read_convention_template'] = [
        'template' => 'read-convention',
        'path' => $templatePath
    ];
    
    return $themes;
}

/**
 * Implements template_preprocess_page().
 */
function bootstrap_child_preprocess_page(&$variables) {

  /*print "<pre>";print_r(node_load(87));die();

$n = node_load($node->field_session_refrence['und'][0]['target_id']);
$n->field_name_of_school_if_occupied['und'][0]['target_id'] = $node->uid;
$n->field_name_of_student_if_occupie = $node->field_student_refrence;
node_save($n);*/

}

// function bootstrap_child_form_alter(&$form, &$form_state, $form_id){
  // if( $form_id == 'branches_centers_node_form' ){
    // $form['actions']['submit']['#submit'][0] = 'bootstrap_child_my_custom_submit_handler';
  // }
// }



function send_report_email($nid){
	
	$node_data = node_load($nid);

	$lat = $node_data->location['latitude'];
	$lon = $node_data->location['longitude'];

	$R = 3960;  // earth's mean radius
	$rad = '20';
	// first-cut bounding box (in degrees)
	$maxLat = $lat + rad2deg($rad/$R);
	$minLat = $lat - rad2deg($rad/$R);
	// compensate for degrees longitude getting smaller with increasing latitude
	$maxLon = $lon + rad2deg($rad/$R/cos(deg2rad($lat)));
	$minLon = $lon - rad2deg($rad/$R/cos(deg2rad($lat)));

	$maxLat=number_format((float)$maxLat, 6, '.', '');
	$minLat=number_format((float)$minLat, 6, '.', '');
	$maxLon=number_format((float)$maxLon, 6, '.', '');
	$minLon=number_format((float)$minLon, 6, '.', '');
	$query = "SELECT * FROM `location` WHERE `latitude` BETWEEN $minLat AND $maxLat AND `longitude` BETWEEN $minLon AND $maxLon ORDER BY `longitude` ASC";
	$result = db_query($query);

	foreach ($result as $row) {
		$rows[] = $row->lid;
	}
	$rows = join(",",$rows); 
	$query1 = "SELECT nid FROM location_instance WHERE lid IN ($rows);";
	$result1 = db_query($query1);

	foreach ($result1 as $row1) {
		if($row1->nid > 0){
			$node_data = node_load($row1->nid);
			$uid = $node_data->uid;
			$user_fields = user_load($uid);
			$email = $user_fields->mail;
			
			$data = "here is a news";
			$params = array('current_conditions' => $data);
			$to = $email;
			$message = drupal_mail('example', 'notice', $to, $language, $params, FALSE);
			// Only add to the spool if sending was not canceled.
			if ($message['send']) {
				example_spool_message($message);
			}
			print_r($email);	
		}
		
	}
}


