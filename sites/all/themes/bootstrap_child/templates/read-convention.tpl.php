<?php 

    global $user;
    $node = node_load(arg(2));
    $entityId = arg(3);
    
    $found = false;
    $justSigned = isset($_GET['signed']) && $_GET['signed'] == 1;
    $isSchool = is_array($user->roles) && in_array('school', $user->roles);
    $isEnterprise = is_array($user->roles) && in_array('enterprise', $user->roles);
    $isAdmin = is_array($user->roles) && in_array('administrator', $user->roles);
    $hasSigned = false;

    $wrapper = entity_metadata_wrapper('node', $node);
    
    $class = $node->field_classe[LANGUAGE_NONE][0]['value'];
    
    $school = $wrapper->field_name_of_school_if_occupied->value();
    $schoolName = $school->name;
    $schoolHasSign = $node->field_signature[LANGUAGE_NONE][0]['value'] == 1;
    
    $students = $wrapper->field_name_of_student_if_occupie->value();
    
    $sessionTerm = taxonomy_term_load($node->field_schedule[LANGUAGE_NONE][0]['tid']);
    $session = $sessionTerm->name;
    
    $enterprises = [];
    $establishmentHasSigned = false;
    $enterprisesFields = $node->field_enterprises[LANGUAGE_NONE];
    $enterprisesValues = [];
    foreach($enterprisesFields as $f) {
        $enterprisesValues[] = $f['value'];
    }
    $enterprisesTmp = entity_load('field_collection_item', $enterprisesValues);
    foreach($enterprisesTmp as $e) {
        $wrapperEnterprise = entity_metadata_wrapper('field_collection_item', $e);
        $branch = $wrapperEnterprise->field_lieu_de_stage->value();
        $enterpriseUser = user_load($branch->uid);
        $enterpriseName = $enterpriseUser->name;
        
        if($e->item_id == $entityId) {
            $id = $entityId;
            $hasSigned = $e->field_signature[LANGUAGE_NONE][0]['value'] == 1;
            $found = true;
        }
        
        $profile = profile2_load_by_user($branch->uid, 'enterprise');
        if($profile && isset($profile->field_name_of_the_enterprise)) {
            $enterpriseName = $profile->field_name_of_the_enterprise[LANGUAGE_NONE][0]['value'];
        }
        
        $enterprises[] = (object) [
            'name' => $enterpriseName,
            'branch' => $branch->title,
            'hasSign' => $e->field_signature[LANGUAGE_NONE][0]['value'] == 1
        ];
    }
    
    if($profile && isset($profile->field_nom_de_l_cole)) {
        $schoolNameII = $profile->field_nom_de_l_cole[LANGUAGE_NONE][0]['value'];
    }
    
    if($school->uid === $user->uid) {
        $id = $school->uid;
        $found = true;
    }
    
    if($schoolHasSign && $isSchool) {
        $hasSigned = true;
    }
    
    if(!$found) {
        drupal_access_denied();
    }

?>

<?php if($isAdmin) : ?>
<a href="/node/<?= $node->nid ?>/edit" class="btn btn-default"><i class="fa fa-pencil"></i>Editer la session/convention</a>
<?php endif ?>

<button class="hide-on-print" onclick="window.print()" class="btn btn-default"><i class="fa fa-print"></i> Imprimer</button>

<?php if($justSigned) : ?>
<div class="alert alert-success">
    <i class="fa fa-check"></i> Vous avez signé la convention, merci !
</div>
<?php endif ?>

<h2>Article 1<sup>ER</sup></h2>
<p>La présente convention a pour objet la mise en oeuvre, au bénéfice des élèves de la classe <strong><?= $class ?></strong>, de la période <strong><?= $session ?></strong> réalisées dans le cadre de l'enseignement professionnel.</p>

<h3>Liste des étudiants</h3>
<table>
    <thead>
        <tr>
            <th>Prénom</th>
            <th>Nom de famille</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($students as $student) : ?>
        <tr>
            <td><?= $student->title ?></td>
            <td><?= $student->field_last_name[LANGUAGE_NONE][0]['value'] ?></td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>


<h2>Article 2</h2>
<p>Les stagiaires demeurent durant leur formation en Entreprise sous statut scolaire. Ils restent sous l'autorité et la responsabilité du Chef de l'Établissement scolaire. Ils ne peuvent prétendre à aucune rémunération de l'Entreprise</p>

<p>Ils sont soumis aux règles générales en vigueur de l'Entreprise ou l'organisme d'accueil, notamment en matière de sécurité, d'honoraires et de discipline.</p>

<h2>Article 3</h2>
<p>Le Chef d'Entreprise prend les dispositions nécessaires pour garantir sa responsabilité civile chaque fois qu'elle sera engagée :</p>
<ul>
  <li>soit en souscrivant une assurance particulière garantissant sa responsabilité civile en cas de faute imputable à l'Entreprise à l'égard du stagiaire.</li>
  <li>soit en ajoutant à son contrat déjà souscrit "responsabilité civile entreprise" ou "responsabilité civile professionnelle" un avenant relatif au stagiaire.</li>
</ul>
<p>Le Chef d'Établissement contracte une assurance couvrant la responsabilité civile de l'élève pour les dommages qu'il pourrait causer pendant la durée ou à l'occasion de son stage dans l'Entreprise.</p>

<h2>Article 4</h2>
<p>En application des disposition de l'article L.412-8 et de l'article R.412-4 du Code de la sécurité sociale, les stagiaires bénéficient de la législation sur les accidents du travail. En cas d'accident survenant à l'élève stagiaire, soit au cours du travail, soit au cours du trajet, la procédure de déclaration d'accident incombe dans tous les cas au Responsable de l'Entreprise. Celui-ci l'adressera à la CPAM compétente, par lettre recommandée avec accusé de réception, dans les 48 heures suivant l'accident, non compris les dimanches et jours fériés. Une copie de la déclaration est envoyée sans délai au Chef d'Établissement.</p>

<h2>Article 5</h2>
<p>Les élèves sont associés aux activités de l'Entreprise ou organisme concourant directement à l'action pédagogique. En aucune cas, leur participation à ces activités ne doit porter préjudice à la situation de l'emploi dans l'Entreprise. Ils sont tenus au respect du secret professionnel.</p>

<h2>Article 6</h2>
<p>Le Chef d'Établissement et la Représentant de l'Entreprise ou organisme d'accueil des stagiaires se tiendront mutuellement informés des difficultés qui pourraient naître de l'application de la présente convention et prendront, d'un commun accord et en liaison avec l'équipe pédagogique, les dispositions propores à résoudre les problèmes de manquement à la discipline.</p>

<h2>Article 7</h2>
<p>Chacune des parties ne pourra mettre fin à la présente convention que pour des raisons graves et sérieuses et après en avoir informé l'autre par courrier ou fax.</p>

<hr />

<h3>Signatures</h3>
<table>
    <tbody>
        <tr>
            <th width="66%">Nom de l'école</th>
            <th width="34%">Signature</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $schoolName ?></td>
            <td><?= $schoolHasSign ? '<i class="fa fa-check"></i> Signé' : 'En attente' ?></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <th width="33%">Nom de l'entreprise</th>
            <th width="33%">Lieu de stage</th>
            <th width="34%">Signature</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($enterprises as $enterprise) : ?>
        <tr>
            <td><?= $enterprise->name ?></td>
            <td><?= $enterprise->branch ?></td>
            <td><?= $enterprise->hasSign ? '<i class="fa fa-check"></i> Signé' : 'En attente' ?></td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>

<?php if(($isSchool || $isEnterprise) && !$hasSigned) : ?>
<div class="hide-on-print">
    <hr />
    <h3>Signer la convention</h3>
    <label>Entrer le code de sécurité :</label>
    <input id="signConventionCode" type="text" value="" placeholder="Code de sécurité" />
    
    <a href="/<?= $isSchool ? 'establishments' : 'enterprises' ?>/<?= $node->nid ?>/sign-convention/<?= $id ?>" id="signConvention" class="btn btn-success"><i class="fa fa-check"></i> J'accepte la présente convention</a>
</div>
<?php endif ?>
