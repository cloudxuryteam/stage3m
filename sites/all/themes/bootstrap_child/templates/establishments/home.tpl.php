<?php

$menu = '';
$menuItems = menu_tree_page_data('main-menu'); 
foreach($menuItems as $key => $m) {
   if ($m['link']['in_active_trail'] && $menuItems[$key]['below']) {
       $menuTree = menu_tree_output($menuItems[$key]['below']);
       $menu = render($menuTree);
   }  
}
print $menu;

?>