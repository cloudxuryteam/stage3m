(function($) {
    
    $(document).ready(function() {
       
       $('#signConvention').click(function() {
           
            var answer = confirm('Êtes-vous certain de vouloir signer la convention? Cette action ne pourra pas être annulé par la suite');
            if(answer) {
                var code = $('#signConventionCode').val();
               
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('href') + '/' + (code || 'undefined'),
                    dataType: 'json',
                    success: function(response) {
                        
                        if(!response) {
                            alert('Code invalide');
                            return;
                        }
                        
                        window.location.href = window.location.href + '?signed=1';
                    },
                });
            };
            
            return false;
       });
       
       $('[ajax-refresh]').click(function() {
           
            $.ajax({
                type: $(this).attr('ajax-refresh') || 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                success: function() {
                    
                    window.location.reload();
                },
            });
            
            return false;
       });
    });
})(jQuery);